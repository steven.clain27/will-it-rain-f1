import utils from "$lib/utils/utils";

export class Forecast {
    constructor(data) {
        this.rainChance = Math.round(data['pop'] * 100);
        this.date = data['dt'];
        this.hours = new Date(data['dt'] * 1000);

        if (!isNaN(data['rain'])) {
            this.precipitation = data['rain'];
        }
        if (data['rain'] && data['rain']['1h']) {
            this.precipitation = data['rain']['1h'];
        }
        if (data['precipitation']) {
            this.precipitation = data['precipitation'];
        }
    }

    get formattedHours() {
        return utils.dateToHours(this.hours);
    }
}