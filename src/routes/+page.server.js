import {OPENWEATHERMAP_KEY} from '$env/static/private'
/*import race_json from "$lib/server/data/race.json";
import weather_json from "$lib/server/data/weather.json";*/

export const load = async ({fetch}) => {
    let race;
    let weather;
    let error = false;
    // mock //
/*    race = race_json;
    weather = weather_json;
    return {race, weather, error};*/
    // mock //

    await getNextRace();
    if (race) {
        let location = race['Circuit']['Location'];
        weather = await getWeather(location['lat'], location['long']);
    }

    async function getNextRace() {
        const apiUrl = 'http://ergast.com/api/f1/current/next.json';

        await fetch(apiUrl)
            .then(response => response.json())
            .then(data => {
                race = data["MRData"]["RaceTable"]["Races"][0];
            })
            .catch(error => {
                error = true;
                console.log('Une erreur s\'est produite lors de la récupération des données de course:', error);
            });
    }

    async function getWeather(latitude, longitude) {
        let result = null;
        const apiUrl = 'https://api.openweathermap.org/data/3.0/onecall?' + new URLSearchParams({
            lat: latitude,
            lon: longitude,
            appid: OPENWEATHERMAP_KEY
        });

        await fetch(apiUrl)
            .then(response => response.json())
            .then(data => {
                result = data;
            })
            .catch(error => {
                error = true;
                console.log('Une erreur s\'est produite lors de la récupération des données météorologiques:', error);
            });
        return result;
    }

    return {race, weather, error}
}